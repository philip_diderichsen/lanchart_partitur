import sqlite3
import lxml
from lxml.html import builder as elem
import pandas as pd
from decimal import Decimal, getcontext
from markupsafe import Markup

# Set Digit precision
getcontext().prec = 3

ROOT_PATH = "/Users/phb514/my_git/lanchart_partitur"

PIXELSPERSECOND = 400


def make_tierselector_ul(df, attr_df, language):
    """Lav ul'en til dropdownmenuen med tiernavnene."""

    def get_vars(attr_name, att_names_df, lang):
        cols = ['real', 'abbr', f'{lang}_trans', f'{lang}_trans_abbr']
        return att_names_df.loc[att_names_df['clean'] == attr_name, cols]

    def make_li(tier, lang):
        real, abbr = tier.iloc[0][f'{lang}_trans'], tier.iloc[0]['abbr']
        return f'<li><label><input id="{abbr}" class="tierchk" type="checkbox" value="{abbr}">{real}</label></li>'

    # Udelad ortografi, speaker, colorcombo_bg, colorcombo_border og colorcombo_fg fra tiernames -
    # det er datakolonner der ikke skal vises som tiers.
    excludelist = ['ortografi', 'speaker', 'colorcombo_bg', 'colorcombo_border', 'colorcombo_fg']
    tiernames = [get_vars(tiername, attr_df, language) for tiername in list(df) if tiername not in excludelist]
    li_elems = '\n'.join([make_li(tier, language) for tier in tiernames])
    return lxml.html.fromstring(f'<ul class="items-cl">{li_elems}</ul>')


def make_tierlabel(tierabbr, tiertransabbr, speakercode, colcomb):
    if tierabbr == 'ortografi':
        stylestr = 'background: {}; border-color: {}; color: {}; line-height: 17px;'
        style = stylestr.format(colcomb["colorcombo_bg"], colcomb["colorcombo_border"], colcomb["colorcombo_fg"])
        first_interval = f'<div class="interval-cl {speakercode} label-cl">' \
                         f'<div class="inner-cl" style="{style}">{speakercode}</div>' \
                         f'</div>'
    else:
        first_interval = f'<div class="interval-cl {tierabbr} label-cl">' \
                         f'<span class="labelstr" data-tier-abbr="{tierabbr}">{tiertransabbr}</span> ' \
                         f'<span class="tier-x-cl">✕</span>' \
                         f'</div>'
    first_int_elem = lxml.html.fromstring(first_interval)
    tierlabel = elem.DIV(elem.CLASS(f'tier-cl {tierabbr}'), first_int_elem)
    return tierlabel


def make_talerlabels(line_df, speakercode, att_names_df, language):
    def get_abbr(colnames, att_df, lang):
        colnames = [t for t in colnames if t != 'ortografi']
        return [('ortografi', 'ortografi')] + [tuple(att_df.loc[att_df['clean'] == x][['abbr', f'{lang}_trans_abbr']].iloc[0]) for x in colnames]

    # colnames = ['ortografi', 'PoS', 'xlength']  # "PoS" med store bogstaver i testdataene.
    # colnames = ['ortografi', 'pos', 'xlength']  # "pos" med små bogstaver - alle attrs er med små i databasen.
    colcomb_cols = ['colorcombo_bg', 'colorcombo_border', 'colorcombo_fg']
    colcomb = line_df.loc[line_df['speaker'] == speakercode, colcomb_cols].drop_duplicates().to_dict('records')[0]
    tierabbrs = get_abbr(list(line_df), att_names_df, language)
    tierlabels = [make_tierlabel(tierabbr, tiertransabbr, speakercode, colcomb) for tierabbr, tiertransabbr in tierabbrs]
    talerlabels = elem.DIV(elem.CLASS(f'talerlabels-cl {speakercode}'), *tierlabels)
    return talerlabels


def make_labeldiv(page_df, att_df, language):
    speakernames = list(page_df['speaker'].unique())
    talerdivs = [make_talerlabels(page_df, speakercode, att_df, language) for speakercode in speakernames]
    labeldiv = elem.DIV(elem.ATTR({'id': 'labels-id'}), elem.CLASS('labels-cl'), *talerdivs)
    return labeldiv


def make_interval(zipped_cols):
    text, xmin, xmax, xlength, starttime, tiername_trans = zipped_cols
    width = (PIXELSPERSECOND * xlength)
    tiername_label = '' if tiername_trans == 'ortografi' else f'{tiername_trans}: '
    regstart, regend = Decimal(xmin) - Decimal(starttime), Decimal(xmax) - Decimal(starttime)
    div = f'<div class="interval-cl" title="{tiername_label}{text}">' \
          f'<span class="inner_intv-cl" data-width="{width}" style="width: {width}px; max-width: {width}px">' \
          f'<span class="inner_inner-cl">' \
          f'<span class="inner_inner_inner-cl"' \
          f'data-word-xmin="{regstart}" data-word-xmax="{regend}"' \
          f'onclick="createWordRegion(this)">' \
          f'{text if text else "&nbsp;"}' \
          f'</span></span></span>' \
          f'</div>'
    return lxml.html.fromstring(div)


def make_tier(tier_df, tiername, lag, att_df, starttime, lang):
    width = PIXELSPERSECOND * lag
    tier_abbr = tier_trans = 'ortografi'
    if tiername != 'ortografi':
        tier_abbr = att_df.loc[att_df['clean'] == tiername].iloc[0]['abbr']
        tier_trans = att_df.loc[att_df['clean'] == tiername].iloc[0][f'{lang}_trans']

    text_values = tier_df[tiername]
    xmins = tier_df['xmin']
    xmaxs = tier_df['xmax']
    xlengths = tier_df['xlength']
    starttimes = [starttime] * len(text_values)
    tiername_trans = [tier_trans] * len(text_values)
    intervals = [make_interval(x) for x in zip(text_values, xmins, xmaxs,
                                               xlengths, starttimes, tiername_trans)]
    lag_interval = f'<div class="inner_intv-cl lag_interval-cl" style="width: {width}px; max-width: {width}px"></div>'
    lag_interval = f'<div class="inner_intv-cl lag_interval-cl" data-width="{width}" style="width: {width}px; max-width: {width}px"></div>'
    tier = elem.DIV(elem.CLASS(f'tier-cl {tier_abbr}'), lxml.html.fromstring(lag_interval), *intervals)
    return tier


def make_taler(line_df, speakercode, lag, att_df, starttime, lang):
    talere = line_df.groupby('speaker')
    taler_df = talere.get_group(speakercode)
    # tiernames = ['ortografi', 'PoS', 'xlength']  # "PoS" med store bogstaver i testdataene.
    # tiernames = ['ortografi', 'pos', 'xlength']  # "pos" med små bogstaver - alle attrs er med små i databasen.
    # TODO: Temmelig ad hoc håndtering af speaker ..?
    tiernames = [tn for tn in list(taler_df) if tn != 'speaker']
    tiers = [make_tier(taler_df, tiername, lag, att_df, starttime, lang) for tiername in tiernames]
    talerdiv = elem.DIV(elem.CLASS(f'taler-cl {speakercode}'), *tiers)
    return talerdiv


def make_line(page_df, att_df, starttime, language):
    def get_speaker_min_xmin(df, speaker):
        return str(df[df['speaker'] == speaker]['xmin'].iloc[0])

    speakernames = list(page_df['speaker'].unique())
    lags = {spk: Decimal(get_speaker_min_xmin(page_df, spk)) for spk in speakernames}
    # int()-gymnastik for at der ikke 'rundes' væk fra maks. 3 decimaler (pga. noget machine precision ..?)
    # TODO - prøv Decimal()
    shifted_lags = {spk: (Decimal(lag) - Decimal(min(lags.values()))) for spk, lag in lags.items()}
    labeldiv = make_labeldiv(page_df, att_df, language)
    talerdivs = [make_taler(page_df, spk, shifted_lags[spk], att_df, starttime, language) for spk in speakernames]
    linediv = elem.DIV(elem.ATTR({'id': 'linjewrapper-id'}), elem.CLASS('linjewrapper-cl'),
                       labeldiv,
                       elem.DIV(elem.ATTR({'id': 'linjescroll-id'}), elem.CLASS('linjescroll-cl'),
                                elem.DIV(elem.CLASS('linje-cl'),
                                         *talerdivs)))
    return linediv


def display_raw_data(df):
    html = f'<div><pre>{df.to_string()}</pre></div>'
    return lxml.html.fromstring(html)


def display_df_as_html(df, att_df, lang):
    """
    :param df: Pandas dataframe med data der skal vises som HTML. (Overskrifter skal oversættes til menneskeformat).
    :param att_df: Pandas dataframe med labels og oversættelser (fra db-tabellen var_names).
    :param lang: Det sprog som tabeloverskrifterne skal oversættes til.
    :return: 'lxml.html.HtmlElement'. HTML-tabel med oversatte overskrifter.
    """
    # Liste af overskrifter der skal oversættes fra clean- til {lang}_trans-versionen.
    # TODO: dato, projname og beskrivelse kan ikke oversættes da de ikke findes i att_df.
    headers = [header for header in list(df) if header not in ['dato', 'projname', 'beskrivelse']]
    header_translations = {head: att_df.loc[att_df['clean'] == head].iloc[0][f'{lang}_trans'] for head in headers}
    # Erstat kolonneoverskrifterne med deres oversættelser fra att_df.
    df = df.rename(columns=header_translations)
    html = df.to_html(index=False, border=0, classes="html_dataframe-cl",
                      formatters={'beskrivelse': lambda x: x})
    return lxml.html.fromstring(html)


def get_html_chunks(df, att_df, speaker_df, starttime, file_df, language):
    def tostr(elm):
        return Markup(lxml.html.tostring(elm, pretty_print=True).decode("utf-8"))

    tier_selector_ul = tostr(make_tierselector_ul(df, att_df, language))
    partitur_line = tostr(make_line(df, att_df, starttime, language))
    speaker_data_pre = tostr(display_df_as_html(speaker_df, att_df, language))
    file_data_pre = tostr(display_df_as_html(file_df, att_df, language))

    return tier_selector_ul, partitur_line, speaker_data_pre, file_data_pre


def test_db(db_cursor):
    db_cursor.execute(f'SELECT * FROM LANCHART_BORNHOLM WHERE ortografi=?', ('yes',))
    colnames = [description[0] for description in db_cursor.description]
    result = db_cursor.fetchall()
    return result, colnames


SPEAKERVARS = 'informanter_koen informanter_foedselsaar informanter_socialklasse rolle talekilde'.split()


def get_data(db_connection, db_table, starttime, endtime, file):
    """Hent data fra sqlite3 og returner dem som en pandas-dataframe."""
    # NB, queryparametre i sekunder Konverteres til ms da det er sådan de ligger i db for at undgå afrundingsfejl
    query = f"""SELECT * from {db_table}
                 WHERE `xmin` >= {Decimal(starttime) * 1000} AND `xmax` <= {Decimal(endtime) * 1000}
                 AND `filename` = "{file}";"""
    dataframe = pd.read_sql_query(query, db_connection)
    # Konverter fra millisekunder tilbage til sekunder. (NB, kun dataframen der sendes til sqlite3, er i ms).
    dataframe = dataframe.copy()
    ms_cols = ['xmin', 'xmax', 'xlength', 'turnmin', 'turnmax', 'turnduration']
    dataframe[ms_cols] = dataframe[ms_cols] / 1000
    # TODO: Temmelig ad hoc håndtering af filename ..?
    return dataframe.drop(['filename'] + SPEAKERVARS, axis=1)


def get_attr_names(db_connection):
    """Hent attributnavne-data inkl. rensede navne og forkortede navne."""
    query = 'SELECT * FROM var_names;'
    df = pd.read_sql_query(query, db_connection)
    return df


def get_speaker_metadata(corpus_label, filename, db_connection):
    """Hent metadata der er knyttet til den enkelte taler."""
    speakervars = ', '.join(SPEAKERVARS)
    query = f"SELECT DISTINCT speaker, {speakervars} FROM {corpus_label} WHERE filename = '{filename}';"
    df = pd.read_sql_query(query, db_connection)
    return df


def get_file_metadata(filename, db_connection):
    """Hent metadata der er knyttet til hele samtalen. (Bemærk format-hack ved samtaletype, som er float i db(??))."""
    filename = filename.replace('.TextGrid', '')
    query = f'''SELECT samt.name as filename, printf("%d", samt.samtaletype) as samtaletype,
                       samt.dato, proj.name as projname, proj.beskrivelse
                FROM samtaler samt, projekter proj
                WHERE samt.projekt = proj.id and samt.name = "{filename}";'''
    df = pd.read_sql_query(query, db_connection)
    return df


def make_partitur(db_name, label, starttime, endtime, filename, language, baseurl, rootpath):
    soundfile = filename.replace('TextGrid', 'wav')
    db_connection = sqlite3.connect(db_name)
    partitur_df = get_data(db_connection, label, starttime, endtime, filename)
    # Adjust start time and end time to actual interval borders in order to align everything correctly
    new_starttime = Decimal(str(partitur_df['xmin'].min()))
    new_endtime = Decimal(str(partitur_df['xmax'].max()))
    audiolen = str(new_endtime - new_starttime)
    audiourl = Markup(f'{baseurl.rstrip("/")}/audioservice?from={new_starttime}&len={audiolen}&file={soundfile}')
    print('audiourl:', audiourl)
    att_df = get_attr_names(db_connection)
    speaker_df = get_speaker_metadata(label, filename, db_connection)
    file_df = get_file_metadata(filename, db_connection)
    # TODO Add MySQL support
    #  brew install mysql-client
    #  echo 'export PATH="/usr/local/opt/mysql-client/bin:$PATH"' >> /Users/phb514/.bash_profile
    #  Ingen kontakt fra Mac'en:
    #  brew install telnet
    #  telnet 130.225.251.142
    #  Trying 130.225.251.142...
    #  telnet: connect to address 130.225.251.142: Operation timed out
    #  telnet: Unable to connect to remote host
    #  Følgende virker fra dgcssapp01fw:
    #  mysql -h 130.225.251.142 -u root informantbase -p
    #  Hjælper ikke:
    #  grant select,insert,update on informantbase.* to 'root'@% identified by '***';
    #  På serveren selv (norsdivweb01fl.unicph.domain) kan jeg connecte vha. mysql -h localhost -u root informantbase -p
    #  Jeg fjerner et par anonyme brugere. Ved ikke om det har nogen effekt.
    #  Men derefter kan jeg komme ind med norsdivweb01fl.unicph.domain eller 130.225.251.142 som host - UDEN pw:
    #  mysql -h norsdivweb01fl.unicph.domain -u root informantbase
    #  Bemærk, intet pw på norsdivweb01fl.unicph.domain: select user, host, password from mysql.user order by user;
    #  Selv om alt i mysql.user ser rigtigt ud for 'root'@'%', kan jeg stadig ikke komme ind udefra.
    db_connection.close()

    tier_sel_ul, partitur_line, speaker_data, file_data = get_html_chunks(partitur_df,
                                                                          att_df,
                                                                          speaker_df,
                                                                          new_starttime,
                                                                          file_df,
                                                                          language)
    return tier_sel_ul, partitur_line, speaker_data, file_data, audiourl, new_starttime, new_endtime
