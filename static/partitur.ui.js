
// Show or hide element with id elementId.
function toggleVisible(elementId, evnt) {
    let targetElement = document.getElementById(elementId);
    if (targetElement.classList.contains('visible'))
        targetElement.classList.remove('visible');
    else
        targetElement.classList.add('visible');
    // Make sure the document click function does not apply inside the tier selector.
    evnt.stopPropagation();
    }

// Hide element with id elementId.
function removeVisible(elementId) {
    let targetElement = document.getElementById(elementId);
    if (targetElement.classList.contains('visible'))
        targetElement.classList.remove('visible');
    }

// Reset tiers.
function tierResetClick() {
    // Remove all tiers (except orthography); uncheck all boxes in tier selector; remove tiers URL param.
    $(".tier-cl:not(.tier-cl.ortografi)").css('display', 'none');
    $(".tierchk").prop('checked', false);
    var my_url = new URL(window.location);
    my_url.searchParams.delete('tiers');
    window.history.replaceState({}, '', my_url);
    }

// Set zoom level
function zoomToValue(wavesurfer, slider_value, px_factor) {
    let slider = document.querySelector('[data-action="zoom"]');
    let intervalzoomfactor = slider_value / 100;
    slider.value = slider_value;
    wavesurfer.zoom(slider_value * px_factor);
    setWidthOnClass('.inner_intv-cl', intervalzoomfactor);
    let canvasLengths = Array.from($( "#waveform > wave > canvas" )).map(elem => Number(elem.style.width.replace('px', '')));
    let canvasLength = canvasLengths.reduce(function(accumulator, val) { return accumulator + val; }, 0)
    }

// Remove all regions
function removeRegions() {
    var regions = document.querySelectorAll(".wavesurfer-region");
    regions.forEach(reg => { reg.remove(); });
    }

// Highlight on and off on tier labels with X symbol.
function highlightX(elmt) { elmt.children(".tier-x-cl").addClass('highlight-x-cl'); }
function lowlightX(elmt) { elmt.children(".tier-x-cl").removeClass('highlight-x-cl'); }

// Remove tier if tier label clicked. Uncheck box in tier selector. Remove tier from URL.
function removeTier(elmt) {
    var targetClass = elmt.children("span:nth-child(1)").attr('data-tier-abbr');
    if (targetClass && $(".tier-cl." + targetClass).css('display') != 'none'){
        $(".tier-cl." + targetClass).css('display', 'none');
        $('#' + targetClass).prop('checked', false);

        // Update URL.
        var curr_url = new URL(window.location);
        var curr_tiers = curr_url.searchParams.get('tiers');
        if (curr_tiers) {
            curr_tier_arr = curr_tiers.split(' ');

            console.log('Removing ' + targetClass + ' from tiers param ...');
            // If only one tier left: Remove tiers param completely.
            if (curr_tier_arr.length == 1) {
                console.log('Removing tiers param altogether ...');
                curr_url.searchParams.delete('tiers');
                window.history.replaceState({}, '', curr_url);
                }
            else {
                var new_tiers_array = curr_tier_arr.filter(elem => elem !== targetClass).join(' ');  // NB, spaces in URLs = "+"
                curr_url.searchParams.set('tiers', new_tiers_array);
                window.history.replaceState({}, '', curr_url);
                }
            }
        }
    }

function reloadPage() {
            location.reload(true);
    }


function addLeftContext() {
    let window_location = new URL(window.location);
    let currentStart = parseFloat(window_location.searchParams.get('start')) || 0;
    let currentEnd = parseFloat(window_location.searchParams.get('end')) || 0;
    let newStart = currentStart - 30;


    if (newStart < 0 || (currentEnd - newStart) > 120) {
        return; // Don't reload if new start time is less than 0 or the resulting span is greater than 120 seconds
    }

    if (window_location.searchParams.get('origstart') == null) {
        let origStart = window_location.searchParams.get('start');
        window_location.searchParams.set('origstart', origStart);
    }

    window_location.searchParams.set('start', newStart);
    window.location.href = window_location.href;
}


function deactivateContextButtonsIfApplicable(){
    // Check if the button should be deactivated after the page has been reloaded
        let window_location = new URL(window.location);
        let currentStart = parseFloat(window_location.searchParams.get('start')) || 0;
        let currentEnd = parseFloat(window_location.searchParams.get('end')) || 0;
        let currentOrigStart = window_location.searchParams.get('origstart');
        let nextStart = currentStart - 30;

        if (nextStart < 0 || (currentEnd - nextStart) > 120) {
            document.getElementById('addleftcontextbutton-id').classList.add('deactivatedbutton-cl');
            document.getElementById('addleftcontextbutton-id').title = 'Konteksten kan ikke udvides mere.';
        }

        if (currentOrigStart == null) {
            document.getElementById('addleftcontextresetbutton-id').classList.add('invisiblebutton-cl');
        }
}


function resetLeftContext() {
    let window_location = new URL(window.location);
    let origStart = window_location.searchParams.get('origstart');
    if (origStart != null) {
        let end = parseFloat(window_location.searchParams.get('end')) || 0;
        let newStart = parseFloat(origStart);
        window_location.searchParams.set('start', origStart);
        window_location.searchParams.delete('origstart');
        window.location.href = window_location.href;
    }
}
