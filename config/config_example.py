# Eksempel på Windows-config: Nathalies configfil.
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    BASEDIR = basedir
    DB_NAME = r'C:\Users\nmp828\Documents\parse_textgrid\parse_textgrids\corpora.db'


class ProductionConfig(Config):
    pass


class DevelopmentConfig(Config):
    ENV = 'development'
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True