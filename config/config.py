
import os

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


class Config(object):
    DEBUG = False
    TESTING = False
    BASEDIR = basedir


class ProductionConfig(Config):
    DB_NAME = '/opt/output/corpora.db'
    AUDIO_DIR = '/opt/audio'  # NB! Denne mappes i docker-compose til '/mnt/N/HUM-NORS-L-KORPUS/Lydoptagelser' via envvar AUDIO_DIR


class DockerConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    DB_NAME = '/opt/output/corpora.db'
    AUDIO_DIR = 'static'


class DevelopmentConfig(Config):
    ENV = 'development'
    DEVELOPMENT = True
    DEBUG = True
    DB_NAME = '/Users/phb514/mygit/korp-setups/setups/lanchart/corpora/sqlitedb/corpora.db'
    AUDIO_DIR = 'static'


class TestingConfig(Config):
    TESTING = True


class SimonConfig(Config):
    ENV = 'development'
    DEVELOPMENT = True
    DEBUG = True
    DB_NAME = '/Users/simonmork/Desktop/Arbejde/corpora2.db'
    AUDIO_DIR = 'static'


config = {
    'prod': ProductionConfig,
    'docker': DockerConfig,
    'dev': DevelopmentConfig,
    'test': TestingConfig,
    'simon': SimonConfig
}
