import unittest
from partitur_app import app


app.config.from_object('config.TestingConfig')


class TestGraf(unittest.TestCase):
    def setUp(self):
        """Set up app for testing"""
        self.app_test = app.test_client()

    def test_http_status_error(self):
        """Test if http status is 404 without parameters and 200 with parameters"""
        res = self.app_test.get('/')
        self.assertEqual(res.status_code, 404)

    def test_http_status_ok(self):
        """Test if http status is 200 with parameters"""
        res = self.app_test.get('/?label=LANCHART_BORNHOLM&starttime=0&endtime=100&filename=bornholm3-15-OKC-1')
        self.assertEqual(res.status_code, 200)

    # TODO Lav http-fejlkoder for forskellige typer fejlscenarier (ingen rækker returneret fra db;
    #  variable søgt som ikke findes i db; osv.
    #def test_html(self):
       # """Test if plot mime type is 'image/png'"""
        #res = self.app_test.get('/plot.png')
        #self.assertEqual(res.mimetype, 'image/png')


if __name__ == '__main__':
    unittest.main()