import os
import re
import logging
import make_partitur as mp
from subprocess import Popen, PIPE, TimeoutExpired
from flask import Flask, render_template, abort, request, Response
from config.config import config

# CREATE APP
app = Flask(__name__)
app.config.from_object(config[os.getenv('FLASK_CONFIG')])
logger = logging.getLogger('werkzeug')
ROOT_PATH = "/Users/simonmork/Desktop/Arbejde/lanchart_partitur"

# Byg et dict af audiopaths til hurtig lookup. Fjern paths med '[' (= bearbejdede kopier af lydfiler).
audiopaths = []
for root, dirs, files in os.walk(app.config['AUDIO_DIR']):
    for fl in files:
        audiopaths.append(os.path.join(root, fl))
audiopaths = [p for p in audiopaths if '[' not in p]
audiopathdict = {os.path.split(p)[1].lower(): p for p in audiopaths}


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>FEJL! Siden findes ikke. Tjek dine parametre</p>", 404


@app.route('/audioservice', methods=['GET'])
def play_audio():
    return get_audio_stream(request)


def get_audio_stream(req):
    """
    Stream audioklip fra fil - således at wavesurfer kan bruge dataene.
    """
    fromtime = req.args.get('from')
    length = req.args.get('len')
    # Sørg for at "+"-tegnet kommer igennem.
    file = req.args.get('file').replace(' ', '+').replace('%2B', '+')

    # Sørg for at parametrene er sikre.
    paramtest = (re.match(r'\d+(\.\d{0,3})?$', fromtime) and
                 re.match(r'\d+(\.\d{0,3})?$', length) and
                 re.match(r'[\w%\.\+-]+$', file))
    if not paramtest:
        abort(500, description="Incorrect parameters.")

    if float(length) > 120:
        abort(500, description="Length too long.")

    audioerror = f'There was a problem fetching audio from {file}.'
    try:
        pth = audiopathdict[file.lower()]
        cmd = f'ffmpeg -ss {fromtime} -t {length} -i "{pth}" -f mp3 -b 128 -v fatal -'
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)

        try:
            outs, errs = proc.communicate(timeout=20)
            if errs or not outs:
                abort(500, description=audioerror + f'\nErrors:\n{errs.decode("utf-8")}')
            return Response(outs, mimetype="audio/mpeg")
        except TimeoutExpired:
            proc.kill()
            abort(500, description="Timeout expired.")

    except KeyError:
        abort(500, description=audioerror)


@app.route('/partitur', methods=['GET'])
def generate_data():
    """Generer renderet partitur.html-template."""
    return render_partitur_template(request)


def render_partitur_template(req):
    """Returner renderet partitur.html-template"""
    # Få parametre fra URL, fx
    # http://localhost:5000/partitur?label=LANCHART_CLARIN&start=60.024&end=66.768&file=clarin3gl-10-AEB%2BAOE%2BBFG%2BLML.TextGrid&lang=en
    # Og med ordmarkering:
    # http://localhost:5000/partitur?label=LANCHART_CLARIN&start=63.119&end=66.719&targetstart=64.066&targetend=64.509&file=clarin3gl-10-AEB%2BAOE%2BBFG%2BLML.TextGrid&lang=en&tiers=xmin+xmax+turnummer
    label = req.args.get('label')
    starttime = req.args.get('start')
    endtime = req.args.get('end')
    targetstart = req.args.get('targetstart')
    targetend = req.args.get('targetend')
    filename = req.args.get('file')
    language = req.args.get('lang') if req.args.get('lang') else 'da'
    logger.info('partiturtest!')
    baseurl = req.host_url
    origstart = req.args.get('origstart')
    if 'lanchartkorp.ku.dk' in baseurl or 'lanchartpartitur.ku.dk' in baseurl:
        baseurl = baseurl.replace('http:', 'https:')

    # Sørg for at parametrene er sikre.
    paramtest = (re.match(r'\w+$', label) and
                 re.match(r'\d+(\.\d{0,3})?$', starttime) and
                 re.match(r'\d+(\.\d{0,3})?$', endtime) and
                 re.match(r'[\w%\.\+-]+$', filename))
    if not paramtest:
        abort(500, description="Incorrect parameters.")

    if float(starttime) < 0:
        abort(500, description="Incorrect parameters. starttime less than zero.")
    elif float(endtime) < 0:
        abort(500, description="Incorrect parameters. endtime less than zero.")
    elif float(starttime) > float(endtime):
        abort(500, description="Incorrect parameters. starttime over endtime.")
    elif float(endtime) - float(starttime) > 120:
        abort(500, description="Incorrect parameters. starttime to endtime range cannot exceed 120 seconds.")
    elif origstart is not None:
        if float(origstart) < 0 or float(origstart) > float(endtime) or float(endtime) - float(origstart) > 120:
            abort(500, description="Incorrect parameters. invalid orig start.")


    if targetstart or targetend:
        if targetstart is None or targetend is None:
            abort(500, description="Incorrect parameters. Both targetstart and targetend needed.")
        elif not (re.match(r'\d+(\.\d{0,3})?$', targetstart) and re.match(r'\d+(\.\d{0,3})?$', targetstart)):
            abort(500, description="Incorrect parameters.")
        elif not float(starttime) <= float(targetstart) <= float(endtime):
            abort(500, description="Incorrect parameters. targetstart out of range.")
        elif not float(starttime) <= float(targetend) <= float(endtime):
            abort(500, description="Incorrect parameters. targetend out of range.")
        elif float(targetstart) >= float(targetend):
            abort(500, description="Incorrect parameters. targetstart greater than or equal to targetend.")
        elif float(targetend) <= float(targetstart):
            abort(500, description="Incorrect parameters. targetend less than or equal to targetstart.")

    # få databasen fra config
    db_name = app.config['DB_NAME']

    partiturdata = mp.make_partitur(db_name=db_name,
                                    label=label,
                                    starttime=starttime,
                                    endtime=endtime,
                                    filename=filename,
                                    language=language,
                                    baseurl=baseurl,
                                    rootpath=app.config['BASEDIR'])
    tier_sel_ul, partitur_line, speaker_data, file_data, audiourl, new_start, new_end = partiturdata

    return render_template('partitur.html', tier_selector_ul=tier_sel_ul, partitur_line=partitur_line,
                           raw_data_speaker=speaker_data, raw_data_file=file_data, audiourl=audiourl,
                           new_start=new_start, new_end=new_end)


# Run in debug mode from terminal (Mac/Linux):
# cd ~/mygit/lanchart_partitur
# FLASK_APP=partitur_app.py FLASK_DEBUG=1 FLASK_CONFIG=dev flask run --host 0.0.0.0

