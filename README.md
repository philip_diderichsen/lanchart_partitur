# Partitur

App med partiturvisning af LANCHART-data.


## Projektstruktur

```
lanchart_partitur
├── README.md
├── config
│   ├── __init__.py
│   ├── config.py
│   └── config_example.py
├── make_partitur.py                         # Funktioner til at opbygge partiturvisningen
├── partitur_app.py                          # Flask-app
├── requirements.txt
├── static
│   ├── clarin3gl-10-AEB+AOE+BFG+LML.wav     # Test-lydfil
│   ├── partitur.ui.js                       # JS-kode til partiturets UI
│   ├── styles.css
│   ├── wavesurfer.js                        # JS til Wavesurfer-funktionalitet
│   ├── wavesurfer.regions.js                # JS til Wavesurfer-funktionalitet
│   └── wavesurfer.spectrogram.js            # JS til Wavesurfer-funktionalitet
├── templates
│   └── partitur.html
├── test_app.py
└── venv                                     # Python virtual environment (ikke i git)
```
## Systemkrav

- Python 3.8 eller højere.
- SQLite3.
- Adgang til korpusdatabase genereret vha. https://bitbucket.org/philip_diderichsen/parse_textgrids
- Kurant `colorcombos.tsv` fra https://bitbucket.org/philip_diderichsen/lanchart_colorcombos


## Kør lokalt

- Tilpas `config.py` til lokalt system. Navnlig skal DB_NAME under DevelopmentConfig sættes til en korrekt path.

```bash
cd ~/mygit/lanchart_partitur
python3 -m venv venv ; source ./venv/bin/activate
pip install --upgrade pip ; pip install -r requirements.txt  # Hvis pip ikke virker, så prøv pip3
FLASK_APP=partitur_app.py FLASK_DEBUG=1 FLASK_CONFIG=dev flask run --host 0.0.0.0
```

Altså: Skift til lanchart_partitur, aktiver virtualenv og installer dependencies, kør Flask med diverse envvars der styrer hvilken Flask-app der køres og hvordan - herunder hvilken konfiguration der bruges (her "dev").

Lydfilen clarin3gl-10-AEB+AOE+BFG+LML.wav er en del af git-repoet. Så hvis sqlite-databasen er på plads (jf. DB_NAME), kan appen nu tilgås lokalt på port 5000 med URL-parametre. Fx:

http://localhost:5000/partitur?label=LANCHART_CLARIN&start=275.844&end=290.767&file=clarin3gl-10-AEB%2BAOE%2BBFG%2BLML.TextGrid&tiers=turnummer+xmin+xmax&lang=en


## Kør i Docker (lokalt)

Partitur-appen indgår i Docker-setuppet "lanchart" i repoet https://github.com/kuhumcst/korp-setups/.

Dvs. koden clones i Dockerfilen setups/lanchart/partitur/Dockerfile - og setups/lanchart/partiturtest/Dockerfile.

Begge steder er der en fil ved navn start.sh, som kører Flask-appen. Den ser sådan ud, og kører Flask-appen på port 5000 inde i containeren:

```bash
#!/bin/bash
cd /opt/partitur/lanchart_partitur
pip3 install -r requirements.txt
FLASK_APP=partitur_app.py FLASK_CONFIG=prod flask run --host 0.0.0.0
```

I setups/lanchart/docker-compose.yml er det specificeret at containeren "partitur" skal eksponere sin port 5000 på host-maskinens port 5005 og containeren "partiturtest" skal eksponere sin port 5000 på host-maskinens port 5006.

Desuden mappes der til to forskellige volumes på host-maskinen: $DB_PATH peger på sqlite-databasen, og $AUDIO_DIR peger på en mappe med de lydfiler der skal kunne vises i Partitur-appen.

Lanchart-setuppet køres sådan, med de to volumes specificeret som envvariable. Herefter encodes dataene i CWB, og trenddiagramdataene indlæses i MySQL:

```bash
cd korp-setups/setups/lanchart
docker-compose down ; DB_PATH=./corpora/sqlitedb AUDIO_DIR=/Users/phb514/mygit/lanchart_partitur/static docker-compose up -d --build
docker-compose exec backend bash /opt/corpora/encodingscripts/encode_all_lanchart_corpora.sh
docker-compose exec backend bash /opt/corpora/encodingscripts/write_trenddiagram_data.sh
```

Herefter kan Korp tilgås på http://localhost:9111.

I LANCHART-korpussets mode-fil til Korp (setups/lanchart/frontend/app/modes/default_mode.js) er der sørget for at tokens i Korp linker til Partitur-appen på en lokal URL når Korp kører på localhost. Klikker man på "vis partitur" fra Korp, vil man derfor komme til partiturvisningen på port 5005, fx følgende (sørg for at gå ud fra testfilen clarin3gl-10-AEB+AOE+BFG+LML lokalt): 

http://localhost:5005/partitur?label=LANCHART_CLARIN&start=63.663&end=66.622&file=clarin3gl-10-AEB%2BAOE%2BBFG%2BLML.TextGrid


Partitur-appen kører også i containeren "partiturtest". Herfra er den eksponeret på port 5006. Her kan man også se partiturvisningen - men der linkes ikke til den fra Korp. Altså fx:

http://localhost:5006/partitur?label=LANCHART_CLARIN&start=63.663&end=66.622&file=clarin3gl-10-AEB%2BAOE%2BBFG%2BLML.TextGrid



## Deploy på server (i Docker)

På serveren (norsdivweb01fl.unicph.domain) deployes Partitur-appen også i Docker som en del af "lanchart"-setuppet.

Docker-setuppet køres fuldstændig som lokalt (se ovenfor), dog peger AUDIO_DIR på en mappe med alle LANCHART-korpussets lydfiler på N-drevet som er mountet på serveren.

Mount N-drevet:

```bash
sudo mount -t cifs -o username=abc123,uid=xxxxxxxxx,sec=ntlmv2,iocharset=utf8,domain=unicph.domain,dir_mode=0700 //unicph.domain/Groupdir/ /mnt/N
```

NB: Brugeren "lanchart" bruges til at køre git, mens "root" kører Docker.

```bash
cd korp-setups/setups/lanchart
su lanchart
git pull
su -
docker-compose down ; DB_PATH=./corpora/sqlitedb AUDIO_DIR=/mnt/N/HUM-NORS-L-KORPUS/Lydoptagelser docker-compose up -d --build
docker-compose exec backend bash /opt/corpora/encodingscripts/encode_all_lanchart_corpora.sh
```

Lokalt på serveren kører de forskellige apps også på localhost:9111 (Korp), localhost:1234 (CWB-backend), localhost:5005 (Partitur) og localhost:5006 (Partiturtest).

Disse mappes til diverse rigtige webadresser vha. reverse proxy i Apache:

- localhost:9111  ==>  https://lanchartkorp.ku.dk
- localhost:1234  ==>  https://lanchartkorp.ku.dk/backend
- localhost:5005  ==>  https://lanchartkorp.ku.dk/partitur, https://lanchartkorp.ku.dk/audioservice
- localhost:5006  ==>  https://lanchartpartitur.ku.dk/partitur, https://lanchartpartitur.ku.dk/audioservice

Det er sat op i hhv. /etc/apache2/sites-available/lanchartkorp.conf og /etc/apache2/sites-available/lanchartpartitur.conf. Apachekonfigurationerne er under svn-versionsstyring.

Kør `systemctl restart apache2` for at få ændringer i konfigurationerne til at slå igennem.

Fra Korp på https://lanchartkorp.ku.dk linkes der nu til partiturvisningen på https://lanchartkorp.ku.dk/partitur.

Partiturtest kører (ikke linket) på https://lanchartpartitur.ku.dk/partitur.


## Pro tip: CSS

For at undgå karambolage med tiernavne (der indsættes diverse steder som navne på CSS-klasser) indeholder alle klasser i CSS-definitionerne bindestreg, som ikke må forekomme i rensede/forkortede tiernavne.